# ASI1-Springboot-and-Security

## Step0
- Contenu de base sur laquelle la sécurité sera mise en place.

## Step1
- Filter et Intercepter
- [Readme](./step1/README.md)
## Step2
- Mise en place d'une première configuration Spring Security
- [Readme](./step2/README.md)

## Step3
- Mise en place d'une authentification (session) permettant de limiter l'accès aux différentes URL
- [Readme](./step3/README.md)
  
## Step4
- Mise en place de l'enregistrement d'utilisateur avec stockage sécurisé du mot de passe (bcrypt)
- [Readme](./step4/README.md)
  
## Step5
- Mise en place de token JWT compatible avec une approche FULL REST.
- [Readme](./step5/README.md)

## Step6
- Mise en place d'autorisation et de roles
- [Readme](./step6/README.md)

## Step7
- Mise en place de oauth2 (+ keycloak)
- [Readme](./step7/README.md)

# References 
- https://spring.io/guides/topicals/spring-security-architecture/
- https://www.toptal.com/java/rest-security-with-jwt-spring-security-and-java
- https://blog.invivoo.com/securiser-application-spring-boot-spring-security/
- https://reflectoring.io/spring-security-password-handling/
- Springboot auth digest : https://blog.alexis-hassler.com/2015/10/06/spring-boot-digest.htmlsprinb
- password encoding: 
  - https://www.baeldung.com/spring-security-registration-password-encoding-bcrypt
  - https://www.baeldung.com/java-password-hashing
  - bcrypt: https://auth0.com/blog/hashing-in-action-understanding-bcrypt/